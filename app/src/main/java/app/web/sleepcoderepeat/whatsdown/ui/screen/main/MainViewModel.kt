package app.web.sleepcoderepeat.whatsdown.ui.screen.main

import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.web.sleepcoderepeat.whatsdown.data.dataclass.User
import app.web.sleepcoderepeat.whatsdown.qiscuscore.QiscusDataSource
import app.web.sleepcoderepeat.whatsdown.qiscuscore.QiscusSource
import app.web.sleepcoderepeat.whatsdown.utils.SingleLiveEvent
import com.qiscus.sdk.chat.core.data.model.QiscusAccount
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom
import rx.subscriptions.CompositeSubscription

class MainViewModel @ViewModelInject constructor(private val qiscusSource:QiscusSource): ViewModel() {

    private val compositeSubscription: CompositeSubscription by lazy { CompositeSubscription() }

    val listChatRoom: ObservableList<QiscusChatRoom> = ObservableArrayList()
    val openRoom = SingleLiveEvent<QiscusChatRoom>()

    fun init(){
        getListChatt()
    }

    fun getListChatt(){
        compositeSubscription.add(
            qiscusSource.getListChatt(object : QiscusDataSource.GetListChattCallback{
                override fun onSuccess(list: List<QiscusChatRoom>) {
                    Log.e(TAG, list.toString())
                    listChatRoom.clear()
                    listChatRoom.addAll(list)
                }

                override fun onError(msg: String?) {
                    msg?.let {
                        Log.e(TAG, it)
                    }
                }
            })
        )
    }


    override fun onCleared() {
        super.onCleared()
        compositeSubscription.unsubscribe()
    }

    companion object{
        private val TAG = MainViewModel::class.simpleName
    }
}