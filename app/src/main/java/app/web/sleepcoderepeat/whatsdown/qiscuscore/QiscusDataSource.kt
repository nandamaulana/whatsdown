package app.web.sleepcoderepeat.whatsdown.qiscuscore

import app.web.sleepcoderepeat.whatsdown.data.dataclass.User
import com.qiscus.sdk.chat.core.data.model.QiscusAccount
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom
import com.qiscus.sdk.chat.core.data.model.QiscusComment
import io.reactivex.disposables.Disposable
import rx.Subscription

interface QiscusDataSource {
    fun registerUser(user: User, callback: RegisterUserCallback): Subscription
    fun getListChatt(callback: GetListChattCallback): Subscription
    fun getChattRoom(user: User, callback: GetChattRoomCallback): Subscription
    fun getMessageRoom(roomId: Long, callback: GetMessageRoomCallback): Subscription
    fun sendMessage(message: QiscusComment, callback: GetSendMessageCallback): Subscription

    interface RegisterUserCallback{
        fun onSuccess()
        fun onError(msg: String?)
    }
    interface GetListChattCallback{
        fun onSuccess(list: List<QiscusChatRoom>)
        fun onError(msg: String?)
    }
    interface GetChattRoomCallback{
        fun onSucces(chattRoom: QiscusChatRoom)
        fun onError(msg: String?)
    }
    interface GetMessageRoomCallback{
        fun onSucces(messageList: List<QiscusComment>)
        fun onError(msg: String?)
    }
    interface GetSendMessageCallback{
        fun onSucces()
        fun onError(msg: String?)
    }
}