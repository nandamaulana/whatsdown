package app.web.sleepcoderepeat.whatsdown.ui.screen.room

import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import app.web.sleepcoderepeat.whatsdown.qiscuscore.QiscusDataSource
import app.web.sleepcoderepeat.whatsdown.qiscuscore.QiscusSource
import com.qiscus.sdk.chat.core.data.model.QiscusComment
import rx.subscriptions.CompositeSubscription

class RoomViewModel @ViewModelInject constructor( var qiscusSource: QiscusSource)
    : ViewModel(){

    private val compositeSubscription: CompositeSubscription by lazy { CompositeSubscription() }
    val listMessage: ObservableList<QiscusComment> = ObservableArrayList()

    fun init(id: Long){
        getListMessage(id)
    }

    private fun getListMessage(id: Long) {
        compositeSubscription.add(
            qiscusSource.getMessageRoom(id, object : QiscusDataSource.GetMessageRoomCallback{
                override fun onSucces(messageList: List<QiscusComment>) {
                    listMessage.clear()
                    listMessage.addAll(messageList)
                }

                override fun onError(msg: String?) {
                    Log.e(TAG, msg)
                }
            })
        )
    }

    fun sendMessage(message: String, id: Long){

        compositeSubscription.add(
            qiscusSource.sendMessage(QiscusComment.generateMessage(id, message), object : QiscusDataSource.GetSendMessageCallback{
                override fun onSucces() {
                    listMessage.clear()
                    getListMessage(id)
                }

                override fun onError(msg: String?) {
                    Log.e(TAG, msg)
                }
            })
        )
    }


    override fun onCleared() {
        super.onCleared()
        compositeSubscription.clear()
    }

    companion object{
        private val TAG = RoomViewModel::class.java.simpleName
    }
}