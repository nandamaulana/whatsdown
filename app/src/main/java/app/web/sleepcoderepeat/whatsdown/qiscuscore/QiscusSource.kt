package app.web.sleepcoderepeat.whatsdown.qiscuscore

import android.content.Context
import android.content.SharedPreferences
import androidx.core.util.Pair
import app.web.sleepcoderepeat.whatsdown.data.dataclass.User
import app.web.sleepcoderepeat.whatsdown.utils.Constants.Companion.CURRENT_USER_SHARED_KEY
import app.web.sleepcoderepeat.whatsdown.utils.getAvatar
import app.web.sleepcoderepeat.whatsdown.utils.toJsonFormat
import com.qiscus.sdk.chat.core.QiscusCore
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom
import com.qiscus.sdk.chat.core.data.model.QiscusComment
import com.qiscus.sdk.chat.core.data.remote.QiscusApi
import dagger.hilt.android.qualifiers.ApplicationContext
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class QiscusSource @Inject constructor(
    @ApplicationContext val context: Context,
    private val sharedPreferences: SharedPreferences
) : QiscusDataSource {

    override fun registerUser(
        user: User,
        callback: QiscusDataSource.RegisterUserCallback
    ): Subscription = QiscusCore.setUser(user.userId, user.userKey)
        .withUsername(user.username).apply {
            user.extras?.let {
                withExtras(it)
            }
            user.avatarUrl?.let {
                withAvatarUrl(it)
            }
        }.save()
        .map {
            User(
                userId = it.email,
                username = it.username,
                avatarUrl = it.avatar.getAvatar()
            )
        }
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe({
            sharedPreferences.apply {
                edit().putString(CURRENT_USER_SHARED_KEY, it.toJsonFormat()).apply()
            }
            callback.onSuccess()
        }, {
            callback.onError(it.localizedMessage)
        })

    override fun getListChatt(callback: QiscusDataSource.GetListChattCallback): Subscription =
        QiscusApi.getInstance()
            .getAllChatRooms(true, false, true, 1, 100)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                it.forEach { data ->
                    QiscusCore.getDataStore().addOrUpdate(data)
                }
                callback.onSuccess(it.filter { chatRoom -> chatRoom.lastComment != null }.toList())
            }, {
                callback.onError(it.localizedMessage)
            })

    override fun getChattRoom(
        user: User,
        callback: QiscusDataSource.GetChattRoomCallback
    ): Subscription {
        val savedChatRoom: QiscusChatRoom? = QiscusCore.getDataStore().getChatRoom(user.userId)

        return QiscusApi.getInstance()
            .chatUser(user.userId, null)
            .doOnNext { chatRoom: QiscusChatRoom? ->
                QiscusCore.getDataStore().addOrUpdate(chatRoom)
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (savedChatRoom == null) {
                    QiscusCore.getDataStore().addOrUpdate(it)
                    callback.onSucces(it)
                } else {
                    callback.onSucces(savedChatRoom)
                }
            }, {
                callback.onError(it.localizedMessage)
            })
    }

    override fun getMessageRoom(
        roomId: Long,
        callback: QiscusDataSource.GetMessageRoomCallback
    ): Subscription {
        return QiscusApi.getInstance().getChatRoomWithMessages(roomId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({ chatRoomListPair: Pair<QiscusChatRoom?, List<QiscusComment>> ->
                chatRoomListPair.second?.let {
                    callback.onSucces(it)
                    return@let
                }
//                callback.onError("No Data")
            }, { callback.onError(it.localizedMessage) })
    }

    override fun sendMessage(
        message: QiscusComment,
        callback: QiscusDataSource.GetSendMessageCallback
    ): Subscription =
        QiscusApi.getInstance().sendMessage(message)
            .subscribeOn(Schedulers.io()) // need to run this task on IO thread
            .observeOn(AndroidSchedulers.mainThread()) // deliver result on main thread or UI thread
            .subscribe({
                callback.onSucces()
            }, { callback.onError(it.localizedMessage) })

}