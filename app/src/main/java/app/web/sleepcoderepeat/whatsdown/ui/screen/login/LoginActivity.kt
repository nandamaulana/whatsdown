package app.web.sleepcoderepeat.whatsdown.ui.screen.login

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import app.web.sleepcoderepeat.whatsdown.R
import app.web.sleepcoderepeat.whatsdown.databinding.ActivityLoginBinding
import app.web.sleepcoderepeat.whatsdown.ui.screen.main.MainActivity
import app.web.sleepcoderepeat.whatsdown.utils.Constants.Companion.CURRENT_USER_SHARED_KEY
import app.web.sleepcoderepeat.whatsdown.utils.Constants.Companion.USER_SHARED_KEY
import app.web.sleepcoderepeat.whatsdown.utils.obtainViewModel
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.qualifiers.ApplicationContext

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {

    lateinit var loginBinding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginBinding = ActivityLoginBinding.inflate(layoutInflater).apply {
            vm = obtainViewModel(LoginViewModel::class.java)
        }
        setContentView(loginBinding.root)
        loginBinding.lifecycleOwner = this

        checkUserLogin()
        setObservable()

    }

    private fun checkUserLogin() {
        applicationContext.getSharedPreferences(USER_SHARED_KEY,Context.MODE_PRIVATE).apply {
            getString(CURRENT_USER_SHARED_KEY,"")?.let {
                if (!it.isBlank()){
                    openHome()
                }
            }
        }
    }

    private fun setObservable() {
        loginBinding.vm?.apply {
            userId.observe(this@LoginActivity,{
                updateFilledStatus()
            })
            userKey.observe(this@LoginActivity,{
                updateFilledStatus()
            })
            displayName.observe(this@LoginActivity,{
                updateFilledStatus()
            })
            message.observe(this@LoginActivity,{
                Toast.makeText(this@LoginActivity,it,Toast.LENGTH_LONG).show()
            })
            openMain.observe(this@LoginActivity,{
                openHome()
            })
        }
    }

    private fun openHome(){
        startActivity(Intent(this@LoginActivity,MainActivity::class.java))
        finish()
    }
}