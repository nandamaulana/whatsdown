package app.web.sleepcoderepeat.whatsdown.data.dataclass

import org.json.JSONObject
import java.io.Serializable

data class User(
    var userId: String = "",
    var userKey: String = "",
    var username: String = "",
    var avatarUrl: String? = "",
    var extras: JSONObject? = JSONObject()
): Serializable