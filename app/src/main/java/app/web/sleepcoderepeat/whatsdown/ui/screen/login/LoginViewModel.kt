package app.web.sleepcoderepeat.whatsdown.ui.screen.login

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.web.sleepcoderepeat.whatsdown.data.dataclass.User
import app.web.sleepcoderepeat.whatsdown.qiscuscore.QiscusDataSource
import app.web.sleepcoderepeat.whatsdown.qiscuscore.QiscusSource
import app.web.sleepcoderepeat.whatsdown.utils.SingleLiveEvent
import com.qiscus.sdk.chat.core.data.model.QiscusAccount
import rx.subscriptions.CompositeSubscription

class LoginViewModel @ViewModelInject constructor(private val qiscusSource: QiscusSource) :
    ViewModel() {

    private val compositeSubscription: CompositeSubscription by lazy { CompositeSubscription() }
    val userId = MutableLiveData<String>()
    val userKey = MutableLiveData<String>()
    val displayName = MutableLiveData<String>()

    val isFilled = MutableLiveData<Boolean>()
    val isLoading = MutableLiveData<Boolean>()
    val message = MutableLiveData<String>()
    val openMain = SingleLiveEvent<Boolean>()

    fun updateFilledStatus() {
        isFilled.value =
            !(userId.value.isNullOrBlank() || userKey.value.isNullOrBlank() || displayName.value.isNullOrBlank())
    }

    fun login(){
        val user = User()
        userId.value?.let {
            user.userId = it
        }
        userKey.value?.let {
            user.userKey = it
        }
        displayName.value?.let {
            user.username = it
        }
        setUser(user)
    }

    private fun setUser(user: User) {
        isLoading.value = true
        compositeSubscription.add(
            qiscusSource.registerUser(user,
                object : QiscusDataSource.RegisterUserCallback {
                    override fun onSuccess() {
                        openMain.value = true
                        isLoading.value = false

                    }

                    override fun onError(msg: String?) {
                        message.value = msg
                        isLoading.value = false
                    }
                }
            )
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeSubscription.clear()
    }

    companion object {
        private val TAG = LoginViewModel::class.java.simpleName
    }

}