package app.web.sleepcoderepeat.whatsdown.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.whatsdown.R
import app.web.sleepcoderepeat.whatsdown.ui.screen.main.MainViewModel
import app.web.sleepcoderepeat.whatsdown.ui.screen.room.RoomViewModel
import app.web.sleepcoderepeat.whatsdown.utils.getTimeStringFromDate
import com.qiscus.sdk.chat.core.data.model.QiscusComment
import kotlinx.android.synthetic.main.item_chatt_me.view.*
import kotlinx.android.synthetic.main.item_chatt_user.view.*

class ChattRoomAdapter(
    private var chattList: MutableList<QiscusComment>,
    private val mainViewModel: RoomViewModel
) : RecyclerView.Adapter<ChattRoomAdapter.ViewHolder>() {


    private val TYPE_MY_TEXT = 1
    private val TYPE_OPPONENT_TEXT = 2

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            TYPE_MY_TEXT -> {
                val view = layoutInflater.inflate(R.layout.item_chatt_me, null)
                ViewHolderChatItemMySelf(view)
            }
            else -> {
                val view = layoutInflater.inflate(R.layout.item_chatt_user, null)
                ViewHolderChatItemUser(view)
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val chat = chattList[position]
        chat.let {
            if (it.isMyComment) {
                val viewHolderChatItemMySelf = holder as ViewHolderChatItemMySelf
                viewHolderChatItemMySelf.textViewDateTime.text = chat.time.getTimeStringFromDate()
                viewHolderChatItemMySelf.textViewMessage.text = chat.message
            } else {

                val viewHolderChatUser = holder as ViewHolderChatItemUser
                viewHolderChatUser.textViewDateTime.text = chat.time.getTimeStringFromDate()
                viewHolderChatUser.textViewMessage.text = chat.message
            }
        }
    }

    override fun getItemCount(): Int = chattList.size

    override fun getItemViewType(position: Int): Int {
        if (chattList[position].isMyComment) {
            return TYPE_MY_TEXT
        }
        return TYPE_OPPONENT_TEXT
    }

    open inner class ViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView)

    inner class ViewHolderChatItemMySelf constructor(itemView: View) : ViewHolder(itemView) {

        val textViewDateTime: TextView =
            itemView.rootView.text_view_date_time_item_layout_chat_my_self
        val textViewMessage: TextView = itemView.rootView.text_view_message_item_layout_chat_my_self

    }

    inner class ViewHolderChatItemUser constructor(itemView: View) : ViewHolder(itemView) {

        val textViewDateTime: TextView = itemView.rootView.text_view_message_item_layout_chat_user
        val textViewMessage: TextView = itemView.rootView.text_view_message_item_layout_chat_user

    }


    fun replaceData(dataList: MutableList<QiscusComment>) {
        chattList.clear()
        notifyDataSetChanged()
        setList(dataList)
    }

    private fun setList(dataList: MutableList<QiscusComment>) {
        this.chattList = dataList
        notifyDataSetChanged()
    }
}