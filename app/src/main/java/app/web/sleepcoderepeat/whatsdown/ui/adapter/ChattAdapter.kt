package app.web.sleepcoderepeat.whatsdown.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.whatsdown.R
import app.web.sleepcoderepeat.whatsdown.databinding.ItemChattBinding
import app.web.sleepcoderepeat.whatsdown.ui.listener.ChattClickListener
import app.web.sleepcoderepeat.whatsdown.ui.screen.main.MainViewModel
import app.web.sleepcoderepeat.whatsdown.utils.getAvatar
import app.web.sleepcoderepeat.whatsdown.utils.toLastMessageTimeStamp
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom

class ChattAdapter(
    private var chattList: MutableList<QiscusChatRoom>,
    private val mainViewModel: MainViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ChattViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_chatt,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        chattList[position].apply {
            (holder as ChattViewHolder).bind(this, object : ChattClickListener {
                override fun onChattClick() {
                    mainViewModel.openRoom.value = this@apply
                }
            })
        }
    }

    override fun getItemCount(): Int = chattList.size


    fun replaceData(dataList: MutableList<QiscusChatRoom>) {
        setList(dataList)
    }

    private fun setList(dataList: MutableList<QiscusChatRoom>) {
        this.chattList = dataList
        notifyDataSetChanged()
    }

    class ChattViewHolder(val binding: ItemChattBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(qiscusChatRoom: QiscusChatRoom, listener: ChattClickListener) {
            qiscusChatRoom.avatarUrl = qiscusChatRoom.avatarUrl.getAvatar()
            binding.lastChatt = qiscusChatRoom.lastComment.time.toLastMessageTimeStamp()
            binding.chattItem = qiscusChatRoom
            binding.action = listener
            binding.executePendingBindings()
        }
    }
}