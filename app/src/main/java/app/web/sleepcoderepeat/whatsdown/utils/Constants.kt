package app.web.sleepcoderepeat.whatsdown.utils

import java.util.*
import kotlin.collections.HashMap

class Constants {
    companion object {
        const val CURRENT_USER_SHARED_KEY = "current_user"
        const val USER_SHARED_KEY = "user"
        const val CHATT_ROOM_SHARED_KEY = "chatt_room"
    }
}