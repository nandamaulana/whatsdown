package app.web.sleepcoderepeat.whatsdown.ui

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.whatsdown.ui.adapter.ChattAdapter
import app.web.sleepcoderepeat.whatsdown.ui.adapter.ChattRoomAdapter
import app.web.sleepcoderepeat.whatsdown.utils.load
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom
import com.qiscus.sdk.chat.core.data.model.QiscusComment

object GlobalBinding {

    @BindingAdapter("loadImage")
    @JvmStatic
    fun setImage(imageView: ImageView, url: String?) {
        url?.let {
            imageView.load(it)
        }
    }

    @BindingAdapter("chattRoomList")
    @JvmStatic
    fun setChattRoomList(recyclerView: RecyclerView, dataList: MutableList<QiscusChatRoom>) {
        if (dataList.size != 0)
            (recyclerView.adapter as ChattAdapter).replaceData(dataList)
    }
    @BindingAdapter("messageList")
    @JvmStatic
    fun setMessageList(recyclerView: RecyclerView, dataList: MutableList<QiscusComment>) {
        if (dataList.size != 0)
            (recyclerView.adapter as ChattRoomAdapter).replaceData(dataList)
    }
}