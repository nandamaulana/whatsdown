package app.web.sleepcoderepeat.whatsdown.ui.screen.room

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import app.web.sleepcoderepeat.whatsdown.databinding.ActivityRoomBinding
import app.web.sleepcoderepeat.whatsdown.ui.adapter.ChattRoomAdapter
import app.web.sleepcoderepeat.whatsdown.utils.Constants.Companion.CHATT_ROOM_SHARED_KEY
import app.web.sleepcoderepeat.whatsdown.utils.obtainViewModel
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom
import com.qiscus.sdk.chat.core.event.QiscusCommentReceivedEvent
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_room.*
import org.greenrobot.eventbus.Subscribe


@AndroidEntryPoint
class RoomActivity : AppCompatActivity() {

    private lateinit var roomBinding: ActivityRoomBinding
    private lateinit var chatRoom: QiscusChatRoom

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        chatRoom = intent.getParcelableExtra(CHATT_ROOM_SHARED_KEY) as QiscusChatRoom
        roomBinding = ActivityRoomBinding.inflate(layoutInflater).apply {
            vm = obtainViewModel(RoomViewModel::class.java)
            chattRoom = chatRoom
        }
        setContentView(roomBinding.root)
        roomBinding.lifecycleOwner = this
        setSupportActionBar(roomBinding.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        roomBinding.vm?.init(chatRoom.id)
        setupRecyclerView()
        button_send.setOnClickListener {
            roomBinding.vm?.sendMessage(field_message.text.toString(), chatRoom.id)
        }
        roomBinding.fieldMessage.apply {
            requestFocus()
            clearFocus()
        }
    }

    private fun setupRecyclerView() {
        roomBinding.vm?.apply {
            roomBinding.rvUserChatt.adapter = ChattRoomAdapter(this.listMessage, this)
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home ->
                onBackPressed()
        }
        return super.onOptionsItemSelected(item)

    }

}