package app.web.sleepcoderepeat.whatsdown.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import app.web.sleepcoderepeat.whatsdown.utils.Constants.Companion.USER_SHARED_KEY
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class LocalDataComponent {

    @Singleton
    @Provides
    fun provideSharedPreference(@ApplicationContext context: Context): SharedPreferences{
        return context.getSharedPreferences(USER_SHARED_KEY,Context.MODE_PRIVATE)
    }
}