package app.web.sleepcoderepeat.whatsdown.utils

import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import app.web.sleepcoderepeat.whatsdown.R
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import java.io.Serializable
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


fun ImageView.load(url: String){
    Glide.with(context)
        .load(url)
        .placeholder(R.drawable.no_image)
        .error(R.drawable.no_image)
        .into(this)
}

fun <T : ViewModel> AppCompatActivity.obtainViewModel(viewModelClass: Class<T>) =
    ViewModelProvider(this).get(viewModelClass)

fun Serializable.toJsonFormat(): String{
    return Gson().toJson(this)
}

@Throws(JsonSyntaxException::class)
fun <T> String.fromJsonTo(dataClass: Class<T>): T{
    return Gson().fromJson(this, dataClass)
}

fun String.getAvatar(): String{
    val avatar = this.replace(" ".toRegex(), "")
    return "https://robohash.org/$avatar/bgset_bg2/3.14160?set=set4"
}

fun Date.toLastMessageTimeStamp(): String {
    return run {
        val todayCalendar = Calendar.getInstance()
        val localCalendar = Calendar.getInstance()
        localCalendar.time = this
        when {
            todayCalendar.time.getDateStringFromDate() == localCalendar.time.getDateStringFromDate() -> {
                this.getTimeStringFromDate()
            }
            todayCalendar[Calendar.DATE] - localCalendar[Calendar.DATE] === 1 -> {
                "Yesterday"
            }
            else -> {
                this.getDateStringFromDate()
            }
        }
    }
}

fun Date.getDateStringFromDate(): String{
    val dateFormat: DateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.US)
    return dateFormat.format(this)
}

fun Date.getTimeStringFromDate(): String {
    val dateFormat: DateFormat = SimpleDateFormat("HH:mm", Locale.US)
    return dateFormat.format(this)

}