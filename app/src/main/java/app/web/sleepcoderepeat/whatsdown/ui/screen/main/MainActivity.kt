package app.web.sleepcoderepeat.whatsdown.ui.screen.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import app.web.sleepcoderepeat.whatsdown.R
import app.web.sleepcoderepeat.whatsdown.databinding.ActivityMainBinding
import app.web.sleepcoderepeat.whatsdown.ui.adapter.ChattAdapter
import app.web.sleepcoderepeat.whatsdown.ui.screen.room.RoomActivity
import app.web.sleepcoderepeat.whatsdown.ui.screen.login.LoginActivity
import app.web.sleepcoderepeat.whatsdown.utils.Constants.Companion.CHATT_ROOM_SHARED_KEY
import app.web.sleepcoderepeat.whatsdown.utils.Constants.Companion.USER_SHARED_KEY
import app.web.sleepcoderepeat.whatsdown.utils.obtainViewModel
import com.qiscus.sdk.chat.core.QiscusCore
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var mainBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = ActivityMainBinding.inflate(layoutInflater).apply {
            vm = obtainViewModel(MainViewModel::class.java)
        }
        setContentView(mainBinding.root)
        mainBinding.lifecycleOwner = this
        setSupportActionBar(mainBinding.toolbar)
        supportActionBar?.apply {
            setDisplayShowTitleEnabled(false)
        }

        setUpRecyclerView()
        setUpObserver()

    }

    private fun setUpRecyclerView() {
        mainBinding.vm?.apply {
            mainBinding.rvChatt.adapter = ChattAdapter(this.listChatRoom, this)
        }
    }

    private fun setUpObserver() {
        mainBinding.vm?.apply {
            openRoom.observe(this@MainActivity,{
                val intent = Intent(this@MainActivity, RoomActivity::class.java)
                intent.putExtra(CHATT_ROOM_SHARED_KEY,it)
                startActivity(intent)
            })
            init()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.logout -> {
                QiscusCore.clearUser()
                applicationContext.getSharedPreferences(USER_SHARED_KEY, Context.MODE_PRIVATE)
                    .edit()
                    .clear()
                    .apply()
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}