package app.web.sleepcoderepeat.whatsdown

import android.app.Application
import com.qiscus.sdk.chat.core.QiscusCore
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        QiscusCore.setup(this, BuildConfig.QISCUS_SDK_APP_ID)
    }
}